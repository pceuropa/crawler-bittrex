import logging

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        filename='/home/m/Desktop/localhost/macierze/python/myAPPs/bitcoin-exchange/bittrex/file.log', level=logging.INFO)


def warning(message):
    logging.warning(message)


def info(message):
    logging.info(message)


def create_logger(nameloger):
    """
        Creates a logging object and returns it
    """
    logger = logging.getLogger(nameloger)
    logger.setLevel(logging.WARNING)
    fh = logging.FileHandler("file.log")
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    # add handler to logger object
    logger.addHandler(fh)
    return logger


if __name__ == "__main__":
    logger = create_logger("test logger")
    logger.warning("test errro exception")
    warning('test')
