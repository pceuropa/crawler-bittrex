from time import sleep
from daemon import Daemon
from log import warning, info
from argparse import ArgumentParser
from models import insert_to_tablename, Volume, Last, OpenBuyOrders, OpenSellOrders
from bittrexLib import Bittrex
from exception_decor import exception

"""
File: crawler.py
Author: Rafal Marguzewicz
Email: info@pceuropa.net
Github: https://github.com/pceuropa/
Description: Bittrex
"""


class Worker(Bittrex):
    time_sleep = 60
    sql_volume = Volume()
    sql_last = Last()
    sql_openbuy = OpenBuyOrders()
    sql_opensell = OpenSellOrders()

    def __init__(self, time_sleep=60):
        Bittrex.__init__(self, None, None)
        self.time_sleep = time_sleep

    @exception
    def change_volume(self, prev, market):
        prev_volume = float(prev['BaseVolume'])
        volume = float(market['BaseVolume'])
        market['change'] = int(volume - prev_volume)
        market['percent'] = market['change'] / prev_volume * 100
        if market['change'] > 4 and abs(market['percent']) >= 3:
            info("insert volume")
            self.sql_volume.insert(market)

    def change_last(self, prev, market):
        prev_last = float(prev['Last'])
        last = float(market['Last'])
        change = last - prev_last
        market['percent'] = change / prev_last * 100
        if abs(market['percent']) >= 4:
            info("insert last")
            self.sql_last.insert(market)

    def change_open_sell_orders(self, prev, market):
        open_sell_orders = int(market['OpenSellOrders'])
        prev_open_sell_orders = int(market['OpenSellOrders'])
        change = open_sell_orders - prev_open_sell_orders
        percent = change / open_sell_orders * 100
        if abs(percent) > 5:
            info("insert opensell")
            self.sql_opensell.insert(market)

    def change_open_buy_orders(self, prev, market):
        prev_open_buy_orders = int(prev['OpenBuyOrders'])
        open_buy_orders = int(market['OpenBuyOrders'])
        market['change'] = open_buy_orders - prev_open_buy_orders
        market['percent'] = market['change'] / prev_open_buy_orders * 100
        if abs(market['percent']) > 5:
            info("insert openbuy")
            self.sql_openbuy.insert(market)

    def work(self):
        n = 3
        count = 0
        prev_markets = []

        while True:
            try:
                markets = self.get_market_summaries_by_symbol()
            except Exception as e:
                warning(e)
                sleep(1)
                continue

            for name in markets:
                market = markets[name]
                insert_to_tablename(market, name)

                if count % n == 0:
                    if count != 0:
                        args = (prev_markets[name], markets[name])
                        self.change_volume(*args)
                        self.change_last(*args)
                        self.change_open_buy_orders(*args)
                        self.change_open_sell_orders(*args)

            if count % n == 0:
                prev_markets = markets
            count += 1
            sleep(self.time_sleep)


class daemon_crawler(Daemon):
    def run(self):
        w = Worker()
        w.work()


if __name__ == "__main__":
    Arg = ArgumentParser()
    Arg.add_argument("service", help="start|stop|restart|run")
    Arg.add_argument("-t", '--time')
    a = Arg.parse_args()
    daemonCrawler = daemon_crawler('pid.pid')

    if 'service' in a:
        if a.service == "run":
            daemonCrawler.run()

        if a.service == "start":
            print("restarting...")
            info("restart")
            daemonCrawler.start()

        if a.service == "stop":
            daemonCrawler.stop()

        if a.service == "nothing":
            w = Worker()
            w.work()
