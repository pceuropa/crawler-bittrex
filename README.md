# crawler-bittrex
git clone https://gitlab.com/pceuropa/crawler-bittrex.git
Deamon crawler to bittrex writen in Python3

In console run as deamon:
- python3 crawler.py start
- python3 crawler.py stop

or run normally in bash
- python3 crawler.py nothing
