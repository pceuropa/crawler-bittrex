from pathlib import Path
from json import load
import yaml


def config(filename):
    try:
        home = Path.home()
        path_str = "{}/{}".format(home, filename)
        return load(open(path_str))
    except Exception as e:
        print(e)


def config_yaml(filename):
    home = Path.home()
    path_str = "{}/{}".format(home, filename)
    return yaml.load(open(path_str))
