import logging
from functools import wraps


def create_logger():
    """ Creates a logging object and returns it """
    logger = logging.getLogger("example_logger")
    logger.setLevel(logging.WARNING)
    # create the logging file handler
    fh = logging.FileHandler("file.log")
    fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(fmt)
    fh.setFormatter(formatter)
    # add handler to logger object
    logger.addHandler(fh)
    return logger


def exception(function):
    """
    A decorator that wraps the passed in function and logs
    exceptions should one occur
    """
    @wraps(function)
    def wrapper(*args, **kwargs):
        logger = create_logger()
        try:
            return function(*args, **kwargs)
        except Exception as e:
            # log the exception
            err = "There was an exception in  " + function.__name__
            logger.exception(err)

            # re-raise the exception
    return wrapper
