from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, Float, DateTime, text
from config_parser import config_yaml
from log import warning, info
import inflection


exchange = 'bittrex'
info("get config from secret.yml")
db = config_yaml("secret.yml")
option_engine = 'postgresql+psycopg2://{user}:{pass}@{host}:{port}/{dbname}'.format(**db[exchange])
engine = create_engine(option_engine)
metadata = MetaData(bind=engine)


def get_table(tablename):
    metadata = MetaData(bind=engine)
    table = Table(tablename, metadata,
        Column('id', Integer, primary_key=True, autoincrement=True),
        Column('high', Float),
        Column('low', Float),
        Column('volume', Float),
        Column('last', Float),
        Column('base_volume', Float),
        Column('timestamp', DateTime, server_default=text('NOW()')),
        Column('bid', Float),
        Column('ask', Float),
        Column('ask', Float),
        Column('open_buy_orders', Integer),
        Column('open_sell_orders', Integer),
        Column('prev_day', Float),
    )
    table.create(engine, checkfirst=True)
    return table


def insert_to_tablename(data, tablename):
    b = {}
    connect = engine.connect()
    table = get_table(tablename)

    for key, value in data.items():
        b[inflection.underscore(key)] = value

    try:
        connect.execute(table.insert(), b)
    except Exception as e:
        connect = engine.connect()
        connect.execute(table.insert(), b)
        warning(e)


class Insert():
    """ insert one record in to database"""

    def sql(self, o, table='usdt-btc'):
        connect = engine.connect()
        sql = 'INSERT INTO `'+ table + '`(`'\
              'bid`, `last`, `ask`, `need_buy`, `need_sell`, `need`, `ratio`, `bough`, `sold`, `volume`,'\
              '`base_volume`, `open_buy_orders`, `open_sell_orders`, `prev_day`, `high`, `low`)'\
            'VALUE' \
            '(%f, %f, %f, %i, %i, %i, %f, %i, %i, %f, %f, %i, %i, %f, %f, %f)' % o
        connect.execute(sql)

    def insert(self, data):
        b = {}
        connect = engine.connect()
        for key, value in data.items():
            b[inflection.underscore(key)] = value
        try:
            connect.execute(self.table.insert(), b)
        except Exception as e:
            connect = engine.connect()
            connect.execute(self.table.insert(), b)
            print(e)


class Volume(Insert):
    """Docstring for Volume. """
    table = Table('volume', metadata,
        Column('id', Integer, primary_key=True, autoincrement=True),
        Column('timestamp', DateTime, server_default=text('NOW()')),
        Column('market_name', String(11)),
        Column('percent', Float),
        Column('change', Float),
        Column('base_volume', Float),
        Column('last', Float)
    )
    table.create(engine, checkfirst=True)


class Last(Insert):
    """Docstring for last """
    table = Table('last', metadata,
        Column('id', Integer, primary_key=True, autoincrement=True),
        Column('timestamp', DateTime, server_default=text('NOW()')),
        Column('market_name', String(11)),
        Column('percent', Float),
        Column('change', Float),
        Column('last', Float),
        Column('base_volume', Float),
    )
    table.create(engine, checkfirst=True)


class OpenSellOrders(Insert):
    """Docstring for last """
    table = Table('open_sell_orders', metadata,
        Column('id', Integer, primary_key=True, autoincrement=True),
        Column('timestamp', DateTime, server_default=text('NOW()')),
        Column('market_name', String(11)),
        Column('percent', Float),
        Column('change', Float),
        Column('open_sell_orders', Integer),
        Column('last', Float),
        Column('base_volume', Float),
    )
    table.create(engine, checkfirst=True)


class OpenBuyOrders(Insert):
    """Docstring for last """
    table = Table('open_buy_orders', metadata,
        Column('id', Integer, primary_key=True, autoincrement=True),
        Column('timestamp', DateTime, server_default=text('NOW()')),
        Column('market_name', String(11)),
        Column('percent', Float),
        Column('change', Float),
        Column('open_buy_orders', Integer),
        Column('last', Float),
        Column('base_volume', Float),
    )
    table.create(engine, checkfirst=True)
