#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
File: bittrex.py
Author: Rafal Marguzewicz
Email: info@pceuropa.net
Github: https://github.com/pceuropa
Description: Console to bittrex
"""

from os import system
from datetime import datetime
from time import sleep, time
from terminaltables import AsciiTable
from terminalcolor import Colors
from bittrexLib import Bittrex
from log import info, warning
import numpy as np


class Client(Bittrex, Colors):
    data = []
    colors = Colors()
    market = 'usdt-bcc'
    last_id = 0
    columns = ['Bid', 'Last', 'Ask', 'need_buy', 'need_sell', 'need', 'ratio', 'bough', 'sold', 'Volume', 'BaseVolume', 'OpenBuyOrders', 'OpenSellOrders', 'PrevDay', 'High', 'Low']
    bittrexUrl = "https://bittrex.com/Market/Index?MarketName={}"

    def __init__(self, market):
        Bittrex.__init__(self, None, None)
        if market is not None:
            self.market = market

    def orderbook(self):
        """ Orderbook - need in future"""
        result = self.get_orderbook(self.market)['result']
        need_buy, need_sell, buy, sell = (0, 0, 0, 0)
        try:
            buy = result['buy'][:9]
            sell = result['sell'][:9]

            for b in buy:
                need_buy += b['Quantity']

            for b in sell:
                need_sell += b['Quantity']

            need_buy, need_sell = (int(need_buy), int(need_sell))
            need = need_buy - need_sell
            ratio = need_buy / need_sell
            return {'need_buy': need_buy, 'need_sell': need_sell, 'need': need, 'ratio': ratio}
        except Exception as e:
            warning(e)

        return {'need_buy': 0, 'need_sell': 0, 'need': 0, 'ratio': 0}

        """time, bid, last, ask, need_buy, need_sell, need, ratio, need_buy, need_sell, volume, rsi, cci"""

    def marketsummary(self):
        """ Market summary """
        return self.get_marketsummary(self.market)['result'][0]

    def markethistory(self):
        """ Market history - how many peoples cry(sold) or are happy(buy)"""

        a = {"last_trade_id": self.last_id,
             "bough": 0,
             "weight_bough": 0,
             "sold": 0,
             "weight_sold": 0,
             "trade": 0}

        try:
            market_history = self.get_market_history(self.market)
            data = market_history['result']

            for i in data:
                if i['Id'] == self.last_id:
                    break
                if i['OrderType'] == 'BUY':
                    a["bough"] += 1
                    a["weight_bough"] += i['Quantity']
                else:
                    a["sold"] += 1
                    a["weight_sold"] += i['Quantity']

            self.last_id = data[0]['Id']
            a["trade"] = a["bough"] - a["sold"]
        except IndexError as e:
            warning(e + data[0])
        return a

    def all(self):
        """ Concact all data to one json"""
        """Orderbook | marketsummary and history"""
        o = self.orderbook()
        o.update(self.markethistory())
        o.update(self.marketsummary())
        return o

    def interval(self, time=6):
        """ Check one market every 6 seconds"""
        system("clear")
        info("start" + self.market)
        print(self.colors.head(self.market))

        while True:
            o = self.all()
            self.data.append(o)
            self.print_row(o)
            sleep(time)

    def mean(self, time=6):
        system("clear")
        info("start" + self.market)
        print(self.colors.head(self.market))
        print(self.columns)
        np.set_printoptions(precision=2, suppress=True)

        while True:
            o = self.all()
            self.data.append(self.return_tuple(o))
            np_data = np.array(self.data[-10:])
            print(np_data.mean(axis=0).tolist())
            sleep(time)

    def return_tuple(self, d):
        a = ['Bid', 'Last', 'Ask', 'need_buy', 'need_sell', 'need', 'ratio', 'bough', 'sold', 'Volume', 'BaseVolume', 'OpenBuyOrders', 'OpenSellOrders', 'PrevDay', 'High', 'Low']
        tuple_data = ()
        for key in a:
            tuple_data = tuple_data + (d[key],)
        return tuple_data

    def print_row(self, data):
        """ Return one row of data """
        c = Colors()

        data["need"] = c.zero(data["need"])
        data["trade"] = c.zero(data["trade"])
        string_format = "{Bid:<10.10} {Last:<10.10} {Ask:<10.10} {need:^5} {ratio:>2.1f} {bough:>2} {sold:>2} {trade:^5} {BaseVolume:.1f} {OpenBuyOrders} {OpenSellOrders}"
        string_format = string_format.format(**data)
        print(string_format)

    def markets(self, type):
        """ Search anomalie on all makrets"""
        system("clear")
        print(self.colors.head(type))
        row = {}

        while True:
            try:
                result = self.get_market_summaries()['result']
            except Exception as e:
                warning(e)
                continue

            for i in [x for x in result if "ETH" not in x["MarketName"]]:

                market_name = i["MarketName"]
                date = datetime.fromtimestamp(time()).strftime("%H:%M")
                base_volume = float(i['BaseVolume'])
                last = float(i['Last'])
                open_sell_orders = int(i['OpenSellOrders'])
                open_buy_orders = int(i['OpenBuyOrders'])
                url = self.bittrexUrl.format(market_name)

                change = 0

                try:
                    prev_row = self.data[-1][market_name]
                    prev_volume = float(prev_row['BaseVolume'])
                    prev_last = float(prev_row['Last'])
                    prev_open_buy_orders = float(prev_row['OpenBuyOrders'])
                    prev_open_sell_orders = float(prev_row['OpenSellOrders'])
                except Exception as e:
                    prev_volume = base_volume
                    prev_last = last
                    prev_open_buy_orders = float(i['OpenBuyOrders'])
                    prev_open_sell_orders = float(i['OpenSellOrders'])

                row[market_name] = i

                if type == 'volume':
                    change = int(base_volume - prev_volume)
                    percent = change / prev_volume * 100
                    if change > 4 and abs(percent) >= 2:
                        base_volume = self.colors.head(f"{i['BaseVolume']:.1f}")
                        print(f"{date} {percent:.1f}% {change} {base_volume} {url}")

                if type == 'last':
                    change = last - prev_last
                    percent = change / last * 100
                    if abs(percent) >= 4:
                        last = self.colors.head(f'{last:.8f}')
                        change = self.colors.zero(f'{change:.8f}')
                        print(f'{date} {percent:.2}% {change} {last} {url}')

                if type == 'openbuy':
                    change = open_buy_orders - prev_open_buy_orders
                    percent = change / open_buy_orders * 100
                    change = self.colors.zero(f"{change:.8f}")
                    percent = self.colors.zero(f"{percent:.1f}")
                    if abs(percent) > 3:
                        print(f"{date} {percent:.1f}% {change} {open_buy_orders} {url}")

                if type == 'opensell':
                    change = open_sell_orders - prev_open_sell_orders
                    percent = change / open_sell_orders * 100
                    change = self.colors.zero('{:.8f}'.format(change))
                    percent = self.colors.zero('{:.1f}'.format(percent))
                    if abs(percent) > 3:
                        print(f"{date} {percent:.1f}% {change} {open_sell_orders} {url}")

            self.data.append(row)
            sleep(3 * 60)

    def table(self, table_data):
        """ Return table with data """
        print(AsciiTable(table_data).table)
